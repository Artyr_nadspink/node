import React, { Component } from 'react'
import './App.css'
import {searchTeg,alert} from './lib/fetchAPI'
class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      teg: ''
    }
  }
  onChangeTag= (event) =>{
    const key = event.target.name
    this.setState({[key]:event.target.value})
  }

  onSubmit=(event)=>{
    const response = searchTeg({teg:this.state.teg})
      console.log(this.state.teg)
  }

  render() {
    console.log(this.state.teg)
    return (
      <div className="App">
        <div className="submit">
          <h1>Введите тег для поиска</h1>
          <form onSubmit={this.onSubmit}>
            <input type="text" name="teg" onChange={this.onChangeTag} placeholder="car" value={this.state.teg}/>
            <button type="submit">искать</button>
          </form>
        </div>
      </div>
    )
  }
}

export default App
